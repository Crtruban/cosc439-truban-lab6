#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <semaphore.h>
#include <pthread.h>
#include <stdbool.h>
int maxPizza = 13;//Constant for max pizzas
int maxSlice = 8;//Constant for max slices
pthread_mutex_t lock;
pthread_mutex_t nlock;
pthread_mutex_t flock;
pthread_mutex_t llock;
sem_t mutex;
int slice = 1;//Initial slice count
int pizza = 1;//Initial pizza count
bool first = true;
bool havePizza = true;
void *worker(void *arg)//Arg is the thread number
{
  while(havePizza == true)
    {
       pthread_mutex_lock(&llock);
      if(pizza >= maxPizza && slice > maxSlice)
	{
	  havePizza = false;
	  pthread_exit(0);
	}
	  pthread_mutex_unlock(&llock);
      eat(arg);//Will increase slice count and sleep thread
       pthread_mutex_lock(&lock);
      if(slice > 8)//If there is no pizza, we need to make a phone call.
	
	{
	  pthread_mutex_lock(&flock);
	  if( first == true && havePizza == true)//Check if student is first
	    
	    {
	       printf("Student %d is ordering a pizza.\n", arg);
	       first = false;
	       pthread_mutex_unlock(&nlock);
	    }
	  else
	    {
	    sleep(rand() % 4);
	  }
	  pthread_mutex_unlock(&flock);
	}
      	pthread_mutex_unlock(&lock);
       
    }
  pthread_exit(0);
}
void eat(int arg)
{
  //If pizza and slices are both above they're appropriate ammount threads will exit
  if(pizza >= maxPizza && slice >= maxSlice)
    {
      havePizza = false;
      pthread_exit(0);
    }
    sleep(rand() % 3);
        pthread_mutex_lock(&lock);//Students are polite, they wait for their turn before going for a slice.
  if(slice <= maxSlice)
  {

  printf("Student %d is having slice #%d from pizza %d.\n", arg,slice, pizza);
  slice += 1;


  }
    pthread_mutex_unlock(&lock);
          
  return NULL;
}
void deliver()
{

  while(pizza<maxPizza){
  pthread_mutex_lock(&nlock);//Only one student can be first so lock it
  pizza ++;
  printf("Pizza %d has been delivered!\n", pizza);
  slice = 1;
  first = true;//Now someone else can be first
  pthread_mutex_unlock(&nlock);
  pthread_mutex_lock(&nlock);
  }
  pthread_exit(0);
}
int main(int argc, char* argv[])
{
  int value = atoi(argv[1]);
  pthread_t id[value];
  pthread_mutex_lock(&nlock);
  int i;
  for (i = 0; i < value; i++)
    {
      pthread_create(&id[i], NULL, &worker, (void *) i+1);
    
    }
  pthread_create(&id[value], NULL, &deliver, (void *)value+1);
   for(i = 0; i < value; i++)
    {
      pthread_join(id[i], NULL);
      }

return 0;
}
